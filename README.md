This project is based on the [campaign manager](https://git.laquadrature.net/la-quadrature-du-net/piphone/campaign)
and will be used to keep track of meetings with representative.

Its intent is not to publish the content of the meeting, but rather keep tracks
of position held by representative to facilitate future meetings and to know
the people you talk too.